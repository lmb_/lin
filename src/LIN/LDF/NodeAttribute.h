/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/LDF/platform.h>

#include <map>
#include <ostream>
#include <string>
#include <vector>

#include <LIN/LDF/ConfigurableFrame.h>
#include <LIN/LDF/RealOrInteger.h>

#include <LIN/LDF/lin_ldf_export.h>

namespace LIN {
namespace LDF {

/** node attributes (attributes_def) */
class LIN_LDF_EXPORT NodeAttribute
{
public:
    NodeAttribute();
    NodeAttribute(uint8_t type, std::string & str);
    NodeAttribute(uint8_t type, std::string & supplierId, std::string & functionId);
    NodeAttribute(uint8_t type, std::string & supplierId, std::string & functionId, std::string & variantId);
    NodeAttribute(uint8_t type, std::vector<std::string> & faultStateSignalNames);
    NodeAttribute(uint8_t type, RealOrInteger & duration);
    NodeAttribute(uint8_t type, std::map<std::string, ConfigurableFrame> & configurableFrames);

    // 0 = undefined
    // 1 = LIN_protocol
    // 2 = configured_NAD
    // 3 = initial_NAD
    // 4 = product_id
    // 5 = response_error
    // 6 = fault_state_signals
    // 7 = P2_min
    // 8 = ST_min
    // 9 = N_As_timeout
    // 10 = N_Cr_timeout
    // 11 = configurable_frames_def
    std::uint8_t type;

    /**
     * LIN_protocol (protocol_version)
     * configured_NAD (diag_address)
     * initial_NAD (diag_address)
     * response_error (signal_name)
     */
    std::string str;

    /** product id - supplier id (supplier_id) */
    std::string supplierId;

    /** product id - function id (function_id) */
    std::string functionId;

    /** product id - variant id (variant) */
    std::string variantId;

    /** fault state signals (signal_name) */
    std::vector<std::string> faultStateSignalNames;

    /**
     * P2_min
     * ST_min
     * N_As_timeout
     *  N_Cr_timeout
     */
    RealOrInteger duration; // ms

    /** configurable frames (configurable_frames_def) */
    std::map<std::string, ConfigurableFrame> configurableFrames;
};

std::ostream & operator<<(std::ostream & os, NodeAttribute & obj);

}
}
