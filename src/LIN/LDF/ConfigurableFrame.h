/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/LDF/platform.h>

#include <cstdint>
#include <ostream>
#include <string>

#include <LIN/LDF/lin_ldf_export.h>

namespace LIN {
namespace LDF {

/** configurable frames (configurable_frames_20_def, configurable_frames_21_def) */
class LIN_LDF_EXPORT ConfigurableFrame
{
public:
    ConfigurableFrame();
    ConfigurableFrame(uint8_t type, std::string & name);
    ConfigurableFrame(uint8_t type, std::string & name, std::string & messageId);

    // 0 = undefined
    // 1 = v2.0
    // 2 = v2.1
    uint8_t type;

    /** frame name (frame_name) */
    std::string name;

    /** message id (message_id) */
    std::string messageId; // only v2.0
};

std::ostream & operator<<(std::ostream & os, ConfigurableFrame & obj);

}
}
