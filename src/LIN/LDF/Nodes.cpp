/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/LDF/Nodes.h>

namespace LIN {
namespace LDF {

Nodes::Nodes() :
    masterNodeName(),
    masterTimeBase(),
    masterJitter(),
    slaveNodeNames()
{
}

Nodes::Nodes(std::string & masterNodeName, RealOrInteger & masterTimeBase, RealOrInteger & masterJitter, std::vector<std::string> & slaveNodeNames) :
    masterNodeName(masterNodeName),
    masterTimeBase(masterTimeBase),
    masterJitter(masterJitter),
    slaveNodeNames(slaveNodeNames)
{
}

std::ostream & operator<<(std::ostream & os, Nodes & obj)
{
    os << "Nodes {" << std::endl;

    os << "  Master: " << obj.masterNodeName << ", " << obj.masterTimeBase << " ms, " << obj.masterJitter << " ms;" << std::endl;

    os << "  Slaves: ";
    bool first = true;
    for (std::string & nodeName : obj.slaveNodeNames) {
        if (!first) {
            os << ", ";
        }
        first = false;
        os << nodeName;
    }
    os << ";" << std::endl;

    os << "}" << std::endl;

    return os;
}

}
}
