/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/LDF/Encoding.h>

namespace LIN {
namespace LDF {

Encoding::Encoding() :
    name(),
    values()
{
}

Encoding::Encoding(std::string & name, std::vector<EncodingValue> & values) :
    name(name),
    values(values)
{
}

std::ostream & operator<<(std::ostream & os, Encoding & obj)
{
    os << "  " << obj.name << " {" << std::endl;

    for (EncodingValue & value : obj.values) {
        os << value;
    }

    os << "  }" << std::endl;

    return os;
}

}
}
