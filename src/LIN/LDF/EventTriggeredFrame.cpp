/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/LDF/EventTriggeredFrame.h>

namespace LIN {
namespace LDF {

EventTriggeredFrame::EventTriggeredFrame() :
    name(),
    collisionResolvingScheduleTable(),
    id(),
    frameNames()
{
}

EventTriggeredFrame::EventTriggeredFrame(std::string & name, std::string & collisionResolvingScheduleTable, std::string & id, std::vector<std::string> & frameNames) :
    name(name),
    collisionResolvingScheduleTable(collisionResolvingScheduleTable),
    id(id),
    frameNames(frameNames)
{
}

std::ostream & operator<<(std::ostream & os, EventTriggeredFrame & obj)
{
    os << "  " << obj.name << ": " << obj.collisionResolvingScheduleTable << ", " << obj.id;
    for (std::string & frameName : obj.frameNames) {
        os << ", " << frameName;
    }
    os << ";" << std::endl;

    return os;
}

}
}
