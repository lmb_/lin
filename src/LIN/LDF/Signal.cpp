/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/LDF/Signal.h>

namespace LIN {
namespace LDF {

Signal::Signal() :
    name(),
    size(),
    initValue(),
    publishedBy(),
    subscribedBy()
{
}

Signal::Signal(std::string & name, std::string & size, InitValue & initValue, std::string & publishedBy, std::vector<std::string> & subscribedBy) :
    name(name),
    size(size),
    initValue(initValue),
    publishedBy(publishedBy),
    subscribedBy(subscribedBy)
{
}

std::ostream & operator<<(std::ostream & os, Signal & obj)
{
    os << "  " << obj.name << ": " << obj.size << ", " << obj.initValue << ", " << obj.publishedBy;
    for (std::string & subscribedBy : obj.subscribedBy) {
        os << ", ";
        os << subscribedBy;
    }
    os << ";" << std::endl;

    return os;
}

}
}
