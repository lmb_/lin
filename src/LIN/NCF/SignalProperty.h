/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/NCF/platform.h>

#include <cstdint>
#include <ostream>
#include <string>

#include <LIN/NCF/InitValue.h>

#include <LIN/NCF/lin_ncf_export.h>

namespace LIN {
namespace NCF {

/** signal property (signal_properties) */
class LIN_NCF_EXPORT SignalProperty
{
public:
    SignalProperty();
    SignalProperty(uint8_t type, InitValue & initValue);
    SignalProperty(uint8_t type, std::string & str);

    /** type */
    // 0 = undefined
    // 1 = init_value
    // 2 = size
    // 3 = offset
    // 4 = encoding_name
    uint8_t type;

    /** initial value (init_value) */
    InitValue initValue;

    /**
     * size (size)
     * offset (offset)
     * encoding name (encoding_name)
     */
    std::string str;
};

std::ostream & operator<<(std::ostream & os, SignalProperty & obj);

}
}
