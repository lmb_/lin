/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/NCF/StatusManagement.h>

namespace LIN {
namespace NCF {

StatusManagement::StatusManagement() :
    responseError(),
    faultStateSignals()
{
}

StatusManagement::StatusManagement(std::string & responseError, std::vector<std::string> & faultStateSignals) :
    responseError(responseError),
    faultStateSignals(faultStateSignals)
{
}

std::ostream & operator<<(std::ostream & os, StatusManagement & obj)
{
    os << "  status_management {" << std::endl;

    if (!obj.responseError.empty()) {
        os << "    response_error = " << obj.responseError << ";" << std::endl;
    }

    if (!obj.faultStateSignals.empty()) {
        os << "    fault_state_signals = ";
        bool first = true;
        for (std::string & faultStateSignal : obj.faultStateSignals) {
            if (!first) {
                os << ", ";
            }
            first = false;
            os << faultStateSignal;
        }
        os << ";" << std::endl;
    }

    os << "  }" << std::endl;

    return os;
}

}
}
