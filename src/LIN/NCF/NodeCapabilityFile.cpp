/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/NCF/NodeCapabilityFile.h>

#include <LIN/NCF/Parser.hpp>
#include <LIN/NCF/Scanner.h>

namespace LIN {
namespace NCF {

NodeCapabilityFile::NodeCapabilityFile() :
    languageVersion(),
    nodes(),
    successfullyParsed(false)
{
}

std::ostream & operator<<(std::ostream & os, NodeCapabilityFile & obj)
{
    os << "node_capability_file;" << std::endl;

    if (!obj.languageVersion.empty()) {
        os << "LIN_language_version = " << obj.languageVersion << ";" << std::endl;
    }

    for (std::pair<std::string, Node> node : obj.nodes) {
        os << node.second;
    }

    return os;
}

std::istream & operator>>(std::istream & is, NodeCapabilityFile & obj)
{
    /* Flex scanner */
    Scanner scanner(is);

    /* Bison parser */
    Parser parser(&scanner, &obj);

    /* parse */
    obj.successfullyParsed = (parser.parse() == 0);

    return is;
}

}
}
