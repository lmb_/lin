/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/NCF/platform.h>

#include <cstdint>
#include <map>
#include <ostream>
#include <string>
#include <vector>

#include <LIN/NCF/DiagnosticProperty.h>
#include <LIN/NCF/Encoding.h>
#include <LIN/NCF/Frame.h>
#include <LIN/NCF/GeneralProperty.h>
#include <LIN/NCF/StatusManagement.h>

#include <LIN/NCF/lin_ncf_export.h>

namespace LIN {
namespace NCF {

/** node property (node_definition) */
class LIN_NCF_EXPORT NodeProperty
{
public:
    NodeProperty();
    NodeProperty(uint8_t type, std::vector<GeneralProperty> & generals);
    NodeProperty(uint8_t type, std::vector<DiagnosticProperty> & diagnostics);
    NodeProperty(uint8_t type, std::map<std::string, Frame> & frames);
    NodeProperty(uint8_t type, std::vector<Encoding> & encodings);
    NodeProperty(uint8_t type, StatusManagement & statusManagement);
    NodeProperty(uint8_t type, std::string & freeText);

    /** type */
    // 0 = undefined
    // 1 = general
    // 2 = diagnostic
    // 3 = frames
    // 4 = encoding
    // 5 = status_management
    // 6 = free_text
    uint8_t type;

    /** general definition (general_definition) */
    std::vector<GeneralProperty> generals;

    /** diagnostic definition (diagnostic_definition) */
    std::vector<DiagnosticProperty> diagnostics;

    /** frame definition (frame_definition) */
    std::map<std::string, Frame> frames;

    /** encoding definition (encoding_definition) */
    std::vector<Encoding> encodings;

    /** status management (status_management) */
    StatusManagement statusManagement;

    /** free text definition (free_text_definition) */
    std::string freeText;
};

std::ostream & operator<<(std::ostream & os, NodeProperty & obj);

}
}
