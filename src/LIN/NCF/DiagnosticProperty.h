/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/NCF/platform.h>

#include <cstdint>
#include <ostream>
#include <string>
#include <vector>

#include <LIN/NCF/NodeAddress.h>
#include <LIN/NCF/RealOrInteger.h>

#include <LIN/NCF/lin_ncf_export.h>

namespace LIN {
namespace NCF {

/** diagnostic property (diagnostic_definition) */
class LIN_NCF_EXPORT DiagnosticProperty
{
public:
    DiagnosticProperty();
    DiagnosticProperty(uint8_t type, NodeAddress & nad);
    DiagnosticProperty(uint8_t type, std::string & str);
    DiagnosticProperty(uint8_t type, RealOrInteger & duration);
    DiagnosticProperty(uint8_t type, std::vector<std::string> & supportSids);

    // 0 = undefined
    // 1 = NAD
    // 2 = diagnostic_class
    // 3 = P2_min
    // 4 = ST_min
    // 5 = N_As_timeout
    // 6 = N_Cr_timeout
    // 7 = support_sid
    // 8 = max_message_length
    uint8_t type;

    /** node address (NAD) */
    NodeAddress nad;

    /**
     * diagnostic class (diagnostic_class)
     * max message length (max_message_length)
     */
    std::string str;

    /**
     * (P2_min)
     * (ST_min)
     * (N_As_timeout)
     * (N_Cr_timeout)
     */
    RealOrInteger duration; // ms

    /** support SIDs (support_sid) */
    std::vector<std::string> supportSids;
};

std::ostream & operator<<(std::ostream & os, DiagnosticProperty & obj);

}
}
