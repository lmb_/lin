/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/NCF/NodeAddress.h>

namespace LIN {
namespace NCF {

NodeAddress::NodeAddress() :
    type(0),
    list(),
    rangeFrom(),
    rangeTo()
{
}

NodeAddress::NodeAddress(uint8_t type, std::vector<std::string> & list) :
    type(type),
    list(list),
    rangeFrom(),
    rangeTo()
{
}

NodeAddress::NodeAddress(uint8_t type, std::string & rangeFrom, std::string & rangeTo) :
    type(type),
    list(),
    rangeFrom(rangeFrom),
    rangeTo(rangeTo)
{
}

std::ostream & operator<<(std::ostream & os, NodeAddress & obj)
{
    switch (obj.type) {
    case 1: {
        bool first = true;
        for (std::string & value : obj.list) {
            if (!first) {
                os << ", ";
            }
            first = false;
            os << value;
        }
    }
        break;
    case 2:
        os << obj.rangeFrom << " to " << obj.rangeTo;
        break;
    }

    return os;
}

}
}
