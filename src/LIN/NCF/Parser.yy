%skeleton "lalr1.cc"
%require "3.0"
%defines
%define api.namespace {LIN::NCF}
%define api.token.constructor
%define api.value.type variant
%define parse.assert
%define parse.trace
%define parser_class_name {Parser}
%language "C++"
%locations

    // debug options
%verbose
%debug
%error-verbose

%code requires{
#include <LIN/NCF/Node.h>
namespace LIN {
namespace NCF {
class NodeCapabilityFile;
}
}
}

%lex-param { const LIN::NCF::Parser::location_type & loc }
%parse-param { class Scanner * scanner }
%parse-param { class NodeCapabilityFile * nodeCapabilityFile }

%code{
#include <cstdint>
#include <iostream>
#include <string>
#include <vector>

#include <LIN/NCF/NodeCapabilityFile.h>
#include <LIN/NCF/Scanner.h>

#undef yylex
#define yylex scanner->yylex

#define loc scanner->location
}

    // %destructor { delete($$); ($$) = nullptr; } <node_capability_file>

/* 8.2 Node capability file definition */
%token NODE_CAPABILITY_FILE

/* 8.2.1.1 Node capability language version */
%token LIN_LANGUAGE_VERSION
%type <std::string> language_version

/* 8.2.2 Node definition */
%token NODE
%type <Node> node_definition
%type <std::map<std::string, Node>> node_definition_list
%type <NodeProperty> node_property
%type <std::vector<NodeProperty>> node_property_list
%type <std::string> node_name

/* 8.2.3 General definition */
%token GENERAL LIN_PROTOCOL_VERSION SUPPLIER FUNCTION VARIANT BITRATE SENDS_WAKE_UP_SIGNAL YES NO
%type <std::vector<GeneralProperty>> general_definition
%type <GeneralProperty> general_property
%type <std::vector<GeneralProperty>> general_property_list
%type <bool> yes_no

/* 8.2.3.1 LIN protocol version number definition */
%type <std::string> protocol_version

/* 8.2.3.2 LIN Product Identification */
%type <std::string> supplier_id
%type <std::string> function_id
%type <std::string> variant_id

/* 8.2.3.3 Bit rate */
%token AUTOMATIC MIN MAX SELECT KBPS
%type <Bitrates> bitrate_definition
%type <RealOrInteger> bitrate
%type <std::vector<RealOrInteger>> bitrate_list
%type <RealOrInteger> min_bitrate.opt
%type <RealOrInteger> max_bitrate.opt

/* 8.2.4 Diagnostic definition */
%token DIAGNOSTIC NAD TO DIAGNOSTIC_CLASS P2_MIN ST_MIN N_AS_TIMEOUT N_CR_TIMEOUT MS SUPPORT_SID MAX_MESSAGE_LENGTH
%type <std::vector<DiagnosticProperty>> diagnostic_definition
%type <DiagnosticProperty> diagnostic_property
%type <std::vector<DiagnosticProperty>> diagnostic_property_list
%type <NodeAddress> nad_value

/* 8.2.5 Frame definition */
%token FRAMES PUBLISH SUBSCRIBE
%type <std::map<std::string, Frame>> frame_definition
%type <Frame> single_frame
%type <std::map<std::string, Frame>> single_frame_list
%type <uint8_t> frame_kind
%type <std::string> frame_name

/* 8.2.5.1 Frame properties */
%token LENGTH MIN_PERIOD MAX_PERIOD EVENT_TRIGGERED_FRAME
%type <FrameProperty> frame_property
%type <std::vector<FrameProperty>> frame_property_list

/* 8.2.5.2 Signal definition */
%token SIGNALS SIZE OFFSET INIT_VALUE
%type <std::map<std::string, Signal>> signal_definition signal_definition.opt
%type <Signal> single_signal
%type <std::map<std::string, Signal>> single_signal_list single_signal_list.opt
%type <std::string> signal_name
%type <SignalProperty> signal_property
%type <std::vector<SignalProperty>> signal_property_list
%type <InitValue> init_value
%type <std::string> init_value_scalar
%type <std::vector<std::string>> init_value_array

/* 8.2.5.3 Signal encoding type definition */
%token ENCODING LOGICAL_VALUE PHYSICAL_VALUE BCD_VALUE ASCII_VALUE
%type <std::vector<Encoding>> encoding_definition
%type <Encoding> single_encoding
%type <std::vector<Encoding>> single_encoding_list single_encoding_list.opt
%type <std::string> encoding_name
%type <EncodingValue> encoding_value
%type <std::vector<EncodingValue>> encoding_value_list encoding_value_list.opt
%type <LogicalValue> logical_value
%type <PhysicalRange> physical_range
//%type <void *> bcd_value
//%type <void *> ascii_value
%type <std::string> signal_value
%type <std::string> min_value
%type <std::string> max_value
%type <RealOrInteger> scale
%type <RealOrInteger> offset
%type <std::string> text_info

/* 8.2.6 status management */
%token STATUS_MANAGEMENT RESPONSE_ERROR FAULT_STATE_SIGNALS
%type <StatusManagement> status_management
%type <std::vector<std::string>> fault_state_signals fault_state_signals.opt

/* 8.2.7 Free text definition */
%token FREE_TEXT
%type <std::string> free_text_definition

/* 8.3 Overview of syntax */
%token <std::string> CHAR_STRING IDENTIFIER INTEGER REAL
%type <std::vector<std::string>> identifier_list
%type <std::vector<std::string>> integer_list
%type <RealOrInteger> real_or_integer

/* Punctuators */
%token OPEN_BRACE CLOSE_BRACE SEMICOLON ASSIGN COMMA

%start node_capability_file

%token END 0

%%

    /* 8.2 Node capability file definition */
node_capability_file // not in specification
        : NODE_CAPABILITY_FILE SEMICOLON
          language_version
          node_definition_list { nodeCapabilityFile->languageVersion = $3; nodeCapabilityFile->nodes = $4; }
        ;

    /* 8.2.1.1 Node capability language version */
language_version // language_version in specification
        : LIN_LANGUAGE_VERSION ASSIGN CHAR_STRING SEMICOLON { $$ = $3; }
        ;

    /* 8.2.2 Node definition */
node_definition // node_definition in specification
        : NODE node_name OPEN_BRACE
          node_property_list
          CLOSE_BRACE { $$ = Node($2, $4); }
        ;
node_definition_list // not in specification
        : node_definition { $$ = std::map<std::string, Node>(); $$[$1.name] = $1; }
        | node_definition_list node_definition { $$ = $1; $$[$2.name] = $2; }
        ;
node_property // not in specification
        : general_definition { $$ = NodeProperty(1, $1); }
        | diagnostic_definition { $$ = NodeProperty(2, $1); }
        | frame_definition { $$ = NodeProperty(3, $1); }
        | encoding_definition { $$ = NodeProperty(4, $1); }
        | status_management { $$ = NodeProperty(5, $1); }
        | free_text_definition { $$ = NodeProperty(6, $1); }
        ;
node_property_list // not in specification
        : node_property { $$ = std::vector<NodeProperty>(); $$.push_back($1); }
        | node_property_list node_property { $$ = $1; $$.push_back($2); }
        ;
node_name // node_name in specification
        : IDENTIFIER { $$ = $1; }
        ;

    /* 8.2.3 General definition */
general_definition // general_definition in specification
        : GENERAL OPEN_BRACE
          general_property_list
          CLOSE_BRACE { $$ = $3; }
        ;
general_property // not in specification
        : LIN_PROTOCOL_VERSION ASSIGN protocol_version SEMICOLON { $$ = GeneralProperty(1, $3); }
        | SUPPLIER ASSIGN supplier_id SEMICOLON { $$ = GeneralProperty(2, $3); }
        | FUNCTION ASSIGN function_id SEMICOLON { $$ = GeneralProperty(3, $3); }
        | VARIANT ASSIGN variant_id SEMICOLON { $$ = GeneralProperty(4, $3); }
        | BITRATE ASSIGN bitrate_definition SEMICOLON { $$ = GeneralProperty(5, $3); }
        | SENDS_WAKE_UP_SIGNAL ASSIGN yes_no SEMICOLON { $$ = GeneralProperty(6, $3); }
        ;
general_property_list // not in specification
        : general_property { $$ = std::vector<GeneralProperty>(); $$.push_back($1); }
        | general_property_list general_property { $$ = $1; $$.push_back($2); }
        ;
yes_no // not in specification
        : YES { $$ = true; }
        | NO { $$ = false; }
        ;

    /* 8.2.3.1 LIN protocol version number definition */
protocol_version // protocol_version in specification
        : CHAR_STRING { $$ = $1; }
        ;

    /* 8.2.3.2 LIN Product Identification */
supplier_id // supplier_id in specification
        : INTEGER { $$ = $1; }
        ;
function_id // function_id in specification
        : INTEGER { $$ = $1; }
        ;
variant_id // variant_id in specification
        : INTEGER { $$ = $1; }
        ;

    /* 8.2.3.3 Bit rate */
bitrate_definition // bitrate_definition in specification
        : AUTOMATIC min_bitrate.opt max_bitrate.opt { $$ = Bitrates(1, $2, $3); }
        | SELECT OPEN_BRACE bitrate_list CLOSE_BRACE { $$ = Bitrates(2, $3); }
        | bitrate { $$ = Bitrates(3, $1); }
        ;
bitrate // bitrate in specification
        : real_or_integer KBPS { $$ = $1; }
        ;
bitrate_list // not in specification
        : bitrate { $$ = std::vector<RealOrInteger>(); $$.push_back($1); }
        | bitrate_list COMMA bitrate { $$ = $1; $$.push_back($3); }
        ;
min_bitrate.opt // not in specification
        : %empty { $$ = RealOrInteger(); }
        | MIN bitrate { $$ = $2; }
        ;
max_bitrate.opt // not in specification
        : %empty { $$ = RealOrInteger(); }
        | MAX bitrate { $$ = $2; }
        ;

    /* 8.2.4 Diagnostic definition */
diagnostic_definition // diagnostic_definition in specification
        : DIAGNOSTIC OPEN_BRACE
          diagnostic_property_list
          CLOSE_BRACE { $$ = $3; }
        ;
diagnostic_property // not in specification
        : NAD ASSIGN nad_value SEMICOLON { $$ = DiagnosticProperty(1, $3); }
        | DIAGNOSTIC_CLASS ASSIGN INTEGER SEMICOLON { $$ = DiagnosticProperty(2, $3); }
        | P2_MIN ASSIGN real_or_integer MS SEMICOLON { $$ = DiagnosticProperty(3, $3); }
        | ST_MIN ASSIGN real_or_integer MS SEMICOLON { $$ = DiagnosticProperty(4, $3); }
        | N_AS_TIMEOUT ASSIGN real_or_integer MS SEMICOLON { $$ = DiagnosticProperty(5, $3); }
        | N_CR_TIMEOUT ASSIGN real_or_integer MS SEMICOLON { $$ = DiagnosticProperty(6, $3); }
        | SUPPORT_SID OPEN_BRACE integer_list CLOSE_BRACE SEMICOLON { $$ = DiagnosticProperty(7, $3); }
        | MAX_MESSAGE_LENGTH ASSIGN INTEGER SEMICOLON { $$ = DiagnosticProperty(8, $3); }
        ;
diagnostic_property_list // not in specification
        : diagnostic_property { $$ = std::vector<DiagnosticProperty>(); $$.push_back($1); }
        | diagnostic_property_list diagnostic_property { $$ = $1; $$.push_back($2); }
        ;
nad_value // not in specification
        : integer_list { $$ = NodeAddress(1, $1); }
        | INTEGER TO INTEGER { $$ = NodeAddress(2, $1, $3); }
        ;

    /* 8.2.5 Frame definition */
frame_definition // frame_definition in specification
        : FRAMES OPEN_BRACE
          single_frame_list
          CLOSE_BRACE { $$ = $3; }
        ;
single_frame // single_frame in specification
        : frame_kind frame_name OPEN_BRACE
          frame_property_list
          signal_definition.opt
          CLOSE_BRACE { $$ = Frame($1, $2, $4, $5); }
        ;
single_frame_list // not in specification
        : single_frame { $$ = std::map<std::string, Frame>(); $$[$1.name] = $1; }
        | single_frame_list single_frame { $$ = $1; $$[$2.name] = $2; }
        ;
frame_kind // frame_kind in specification
        : PUBLISH { $$ = 1; }
        | SUBSCRIBE { $$ = 2; }
        ;
frame_name // frame_name in specification
        : IDENTIFIER { $$ = $1; }
        ;

    /* 8.2.5.1 Frame properties */
frame_property // frame_properties in specification
        : LENGTH ASSIGN INTEGER SEMICOLON { $$ = FrameProperty(1, $3); }
        | MIN_PERIOD ASSIGN INTEGER MS SEMICOLON { $$ = FrameProperty(2, $3); }
        | MAX_PERIOD ASSIGN INTEGER MS SEMICOLON { $$ = FrameProperty(3, $3); }
        | EVENT_TRIGGERED_FRAME ASSIGN IDENTIFIER SEMICOLON { $$ = FrameProperty(4, $3); }
        ;
frame_property_list // not in specification
        : frame_property { $$ = std::vector<FrameProperty>(); $$.push_back($1); }
        | frame_property_list frame_property { $$ = $1; $$.push_back($2); }
        ;

    /* 8.2.5.2 Signal definition */
signal_definition // signal_definition in specification
        : SIGNALS OPEN_BRACE
          single_signal_list.opt
          CLOSE_BRACE { $$ = $3; }
        ;
signal_definition.opt // not in specification
        : %empty { $$ = std::map<std::string, Signal>(); }
        | signal_definition { $$ = $1; }
        ;
single_signal // not in specification
        : signal_name OPEN_BRACE signal_property_list CLOSE_BRACE { $$ = Signal($1, $3); }
        ;
single_signal_list // not in specification
        : single_signal { $$ = std::map<std::string, Signal>(); $$[$1.name] = $1; }
        | single_signal_list single_signal { $$ = $1; $$[$2.name] = $2; }
        ;
single_signal_list.opt // not in specification
        : %empty { $$ = std::map<std::string, Signal>(); }
        | single_signal_list { $$ = $1; }
        ;
signal_name // signal_name in specification
        : IDENTIFIER { $$ = $1; }
        ;
signal_property // signal_properties in specification
        : init_value { $$ = SignalProperty(1, $1); }
        | SIZE ASSIGN INTEGER SEMICOLON { $$ = SignalProperty(2, $3); }
        | OFFSET ASSIGN INTEGER SEMICOLON { $$ = SignalProperty(3, $3); }
        | encoding_name SEMICOLON { $$ = SignalProperty(4, $1); }
        ;
signal_property_list // not in specification
        : signal_property { $$ = std::vector<SignalProperty>(); $$.push_back($1); }
        | signal_property_list signal_property { $$ = $1; $$.push_back($2); }
        ;
init_value // init_value in specification
        : init_value_scalar { $$ = InitValue(1, $1); }
        | init_value_array { $$ = InitValue(2, $1); }
        ;
init_value_scalar // init_value_scalar in specification
        : INIT_VALUE ASSIGN INTEGER SEMICOLON { $$ = $3; }
        ;
init_value_array // init_value_array in specification
        : INIT_VALUE ASSIGN OPEN_BRACE integer_list CLOSE_BRACE SEMICOLON { $$ = $4; }
        ;

    /* 8.2.5.3 Signal encoding type definition */
encoding_definition // encoding_definition in specification
        : ENCODING OPEN_BRACE
          single_encoding_list.opt
          CLOSE_BRACE { $$ = $3; }
        ;
single_encoding // not in specification
        : encoding_name OPEN_BRACE
          encoding_value_list.opt
          CLOSE_BRACE { $$ = Encoding($1, $3); }
        ;
single_encoding_list // not in specification
        : single_encoding { $$ = std::vector<Encoding>(); $$.push_back($1); }
        | single_encoding_list single_encoding { $$ = $1; $$.push_back($2); }
        ;
single_encoding_list.opt // not in specification
        : %empty { $$ = std::vector<Encoding>(); }
        | single_encoding_list { $$ = $1; }
        ;
encoding_name // encoding_name in specification
        : IDENTIFIER { $$ = $1; }
        ;
encoding_value // not in specification
        : logical_value { $$ = EncodingValue(1, $1); }
        | physical_range { $$ = EncodingValue(2, $1); }
        | bcd_value { $$ = EncodingValue(3); }
        | ascii_value { $$ = EncodingValue(4); }
        ;
encoding_value_list // not in specification
        : encoding_value { $$ = std::vector<EncodingValue>(); $$.push_back($1); }
        | encoding_value_list encoding_value { $$ = $1; $$.push_back($2); }
        ;
encoding_value_list.opt // not in specification
        : %empty { $$ = std::vector<EncodingValue>(); }
        | encoding_value_list { $$ = $1; }
        ;
logical_value // logical_value in specification
        : LOGICAL_VALUE COMMA signal_value SEMICOLON { $$ = LogicalValue($3); }
        | LOGICAL_VALUE COMMA signal_value COMMA text_info SEMICOLON { $$ = LogicalValue($3, $5); }
        ;
physical_range // physical_range in specification
        : PHYSICAL_VALUE COMMA min_value COMMA max_value COMMA scale COMMA offset SEMICOLON { $$ = PhysicalRange($3, $5, $7, $9); }
        | PHYSICAL_VALUE COMMA min_value COMMA max_value COMMA scale COMMA offset COMMA text_info SEMICOLON { $$ = PhysicalRange($3, $5, $7, $9, $11); }
        ;
bcd_value // bcd_value in specification
        : BCD_VALUE SEMICOLON
        ;
ascii_value // ascii_value in specification
        : ASCII_VALUE SEMICOLON
        ;
signal_value // signal_value in specification
        : INTEGER { $$ = $1; }
        ;
min_value // min_value in specification
        : INTEGER { $$ = $1; }
        ;
max_value // max_value in specification
        : INTEGER { $$ = $1; }
        ;
scale // scale in specification
        : real_or_integer { $$ = $1; }
        ;
offset // offset in specification
        : real_or_integer { $$ = $1; }
        ;
text_info // text_info in specification
        : CHAR_STRING { $$ = $1; }
        ;

    /* 8.2.6 status management */
status_management // status_management in specification
        : STATUS_MANAGEMENT OPEN_BRACE
          RESPONSE_ERROR ASSIGN IDENTIFIER SEMICOLON
          fault_state_signals.opt
          CLOSE_BRACE { $$ = StatusManagement($5, $7); }
        ;
fault_state_signals // not in specification
        : FAULT_STATE_SIGNALS ASSIGN identifier_list SEMICOLON { $$ = $3; }
        ;
fault_state_signals.opt // not in specification
        : %empty { $$ = std::vector<std::string>(); }
        | fault_state_signals { $$ = $1; }
        ;

    /* 8.2.7 Free text definition */
free_text_definition // free_text_definition in specification
        : FREE_TEXT OPEN_BRACE
          CHAR_STRING
          CLOSE_BRACE { $$ = $3; }
        ;

    /* 8.3 Overview of syntax */
identifier_list // not in specification
        : IDENTIFIER { $$ = std::vector<std::string>(); $$.push_back($1); }
        | identifier_list COMMA IDENTIFIER { $$ = $1; $$.push_back($3); }
        ;
integer_list // not in specification
        : INTEGER { $$ = std::vector<std::string>(); $$.push_back($1); }
        | integer_list COMMA INTEGER { $$ = $1; $$.push_back($3); }
        ;
real_or_integer // real_or_integer in specification
        : REAL { $$ = RealOrInteger(1, $1); }
        | INTEGER { $$ = RealOrInteger(2, $1); }
        ;

%%

void LIN::NCF::Parser::error(const location_type & location, const std::string & message)
{
    std::cerr << "Parse error at " << location << ": " << message << std::endl;
}
