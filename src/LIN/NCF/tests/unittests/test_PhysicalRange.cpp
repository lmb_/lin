/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE PhysicalRange
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include <boost/filesystem.hpp>

#include <LIN/NCF/PhysicalRange.h>

/**
 * Check the physical/raw value conversions.
 */
BOOST_AUTO_TEST_CASE(PhysicalRange)
{
    LIN::NCF::PhysicalRange physicalRange;
    physicalRange.scale.value = "1.0";
    physicalRange.offset.value = "0.0";

    /* check for factor and offset */
    BOOST_CHECK_EQUAL(physicalRange.rawToPhysicalValue(1.0), 1.0);
    BOOST_CHECK_EQUAL(physicalRange.physicalToRawValue(1.0), 1.0);
}
