%{
#include <LIN/NCF/NodeCapabilityFile.h>
#include <LIN/NCF/Scanner.h>

/* match end of file */
// #define yyterminate() return LIN::NCF::Parser::make_END
%}

    // debug options
    // %option backup
%option debug
%option perf-report
    // %option trace
    // %option verbose

    // standard options
%option nodefault
%option nounput
%option noinput
%option batch
%option yyclass="LIN::NCF::Scanner"
%option noyywrap
%option c++

DIGIT                   [0-9]
NONDIGIT                [_a-zA-Z]
HEXADECIMAL_PREFIX      (0x|0X)
NONZERO_DIGIT           [1-9]
HEXADECIMAL_DIGIT       [0-9a-fA-F]

%x COMMENT

%%

    /* 8.2 Node capability file definition */
"node_capability_file" {
    return LIN::NCF::Parser::make_NODE_CAPABILITY_FILE(loc); }

    /* 8.2.1.1 Node capability language version */
"LIN_language_version" {
    return LIN::NCF::Parser::make_LIN_LANGUAGE_VERSION(loc); }

    /* 8.2.2 Node definition */
"node" {
    return LIN::NCF::Parser::make_NODE(loc); }

    /* 8.2.3 General definition */
"general" {
    return LIN::NCF::Parser::make_GENERAL(loc); }
"LIN_protocol_version" {
    return LIN::NCF::Parser::make_LIN_PROTOCOL_VERSION(loc); }
"supplier" {
    return LIN::NCF::Parser::make_SUPPLIER(loc); }
"function" {
    return LIN::NCF::Parser::make_FUNCTION(loc); }
"variant" {
    return LIN::NCF::Parser::make_VARIANT(loc); }
"bitrate" {
    return LIN::NCF::Parser::make_BITRATE(loc); }
"sends_wake_up_signal" {
    return LIN::NCF::Parser::make_SENDS_WAKE_UP_SIGNAL(loc); }
"\"yes\"" {
    return LIN::NCF::Parser::make_YES(loc); }
"\"no\"" {
    return LIN::NCF::Parser::make_NO(loc); }

    /* 8.2.3.3 Bit rate */
"automatic" {
    return LIN::NCF::Parser::make_AUTOMATIC(loc); }
"min" {
    return LIN::NCF::Parser::make_MIN(loc); }
"max" {
    return LIN::NCF::Parser::make_MAX(loc); }
"select" {
    return LIN::NCF::Parser::make_SELECT(loc); }
"kbps" {
    return LIN::NCF::Parser::make_KBPS(loc); }

    /* 8.2.4 Diagnostic definition */
"diagnostic" {
    return LIN::NCF::Parser::make_DIAGNOSTIC(loc); }
"NAD" {
    return LIN::NCF::Parser::make_NAD(loc); }
"to" {
    return LIN::NCF::Parser::make_TO(loc); }
"diagnostic_class" {
    return LIN::NCF::Parser::make_DIAGNOSTIC_CLASS(loc); }
"P2_min" {
    return LIN::NCF::Parser::make_P2_MIN(loc); }
"ST_min" {
    return LIN::NCF::Parser::make_ST_MIN(loc); }
"N_As_timeout" {
    return LIN::NCF::Parser::make_N_AS_TIMEOUT(loc); }
"N_Cr_timeout" {
    return LIN::NCF::Parser::make_N_CR_TIMEOUT(loc); }
"ms" {
    return LIN::NCF::Parser::make_MS(loc); }
"support_sid" {
    return LIN::NCF::Parser::make_SUPPORT_SID(loc); }
"max_message_length" {
    return LIN::NCF::Parser::make_MAX_MESSAGE_LENGTH(loc); }

    /* 8.2.5 Frame definition */
"frames" {
    return LIN::NCF::Parser::make_FRAMES(loc); }
"publish" {
    return LIN::NCF::Parser::make_PUBLISH(loc); }
"subscribe" {
    return LIN::NCF::Parser::make_SUBSCRIBE(loc); }

    /* 8.2.5.1 Frame properties */
"length" {
    return LIN::NCF::Parser::make_LENGTH(loc); }
"min_period" {
    return LIN::NCF::Parser::make_MIN_PERIOD(loc); }
"max_period" {
    return LIN::NCF::Parser::make_MAX_PERIOD(loc); }
"event_triggered_frame" {
    return LIN::NCF::Parser::make_EVENT_TRIGGERED_FRAME(loc); }

    /* 8.2.5.2 Signal definition */
"signals" {
    return LIN::NCF::Parser::make_SIGNALS(loc); }
"size" {
    return LIN::NCF::Parser::make_SIZE(loc); }
"offset" {
    return LIN::NCF::Parser::make_OFFSET(loc); }
"init_value" {
    return LIN::NCF::Parser::make_INIT_VALUE(loc); }

    /* 8.2.5.3 Signal encoding type definition */
"encoding" {
    return LIN::NCF::Parser::make_ENCODING(loc); }
"logical_value" {
    return LIN::NCF::Parser::make_LOGICAL_VALUE(loc); }
"physical_value" {
    return LIN::NCF::Parser::make_PHYSICAL_VALUE(loc); }
"bcd_value" {
    return LIN::NCF::Parser::make_BCD_VALUE(loc); }
"ascii_value" {
    return LIN::NCF::Parser::make_ASCII_VALUE(loc); }

    /* 8.2.6 status management */
"status_management" {
    return LIN::NCF::Parser::make_STATUS_MANAGEMENT(loc); }
"response_error" {
    return LIN::NCF::Parser::make_RESPONSE_ERROR(loc); }
"fault_state_signals" {
    return LIN::NCF::Parser::make_FAULT_STATE_SIGNALS(loc); }

    /* 8.2.7 Free text definition */
"free_text" {
    return LIN::NCF::Parser::make_FREE_TEXT(loc); }

    /* 8.3 Overview of syntax */
\"(\\.|[^\\"\n])*\" {
    return LIN::NCF::Parser::make_CHAR_STRING(yytext, loc); }
{NONDIGIT}({NONDIGIT}|{DIGIT})* {
    return LIN::NCF::Parser::make_IDENTIFIER(yytext, loc); }
{DIGIT}+ {
    return LIN::NCF::Parser::make_INTEGER(yytext, loc); }
{HEXADECIMAL_PREFIX}{HEXADECIMAL_DIGIT}+ {
    return LIN::NCF::Parser::make_INTEGER(yytext, loc); }
{DIGIT}*"."{DIGIT}+ {
    return LIN::NCF::Parser::make_REAL(yytext, loc); }

    /* Punctuators */
"{" {
    return LIN::NCF::Parser::make_OPEN_BRACE(loc); }
"}" {
    return LIN::NCF::Parser::make_CLOSE_BRACE(loc); }
";" {
    return LIN::NCF::Parser::make_SEMICOLON(loc); }
"=" {
    return LIN::NCF::Parser::make_ASSIGN(loc); }
"," {
    return LIN::NCF::Parser::make_COMMA(loc); }

    /* Comments */
"/*" {
    BEGIN(COMMENT); }
<COMMENT>([^*]|\n)+|. {
    }
<COMMENT>"*/" {
    BEGIN(INITIAL); }
"//".*\n {
    }

    /* whitespace */
[ \t\v\n\f\r] {
    }

    /* yet unmatched characters */
. {
    std::cerr << "unmatched character: " << *yytext; }

    /* match end of file */
<<EOF>> {
    return LIN::NCF::Parser::make_END(loc); }

%%

