/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/NCF/NodeProperty.h>

namespace LIN {
namespace NCF {

NodeProperty::NodeProperty() :
    type(0),
    generals(),
    diagnostics(),
    frames(),
    encodings(),
    statusManagement(),
    freeText()
{
}

NodeProperty::NodeProperty(uint8_t type, std::vector<GeneralProperty> & generals) :
    type(type),
    generals(generals),
    diagnostics(),
    frames(),
    encodings(),
    statusManagement(),
    freeText()
{
}

NodeProperty::NodeProperty(uint8_t type, std::vector<DiagnosticProperty> & diagnostics) :
    type(type),
    generals(),
    diagnostics(diagnostics),
    frames(),
    encodings(),
    statusManagement(),
    freeText()
{
}

NodeProperty::NodeProperty(uint8_t type, std::map<std::string, Frame> & frames) :
    type(type),
    generals(),
    diagnostics(),
    frames(frames),
    encodings(),
    statusManagement(),
    freeText()
{
}

NodeProperty::NodeProperty(uint8_t type, std::vector<Encoding> & encodings) :
    type(type),
    generals(),
    diagnostics(),
    frames(),
    encodings(encodings),
    statusManagement(),
    freeText()
{
}

NodeProperty::NodeProperty(uint8_t type, StatusManagement & statusManagement) :
    type(type),
    generals(),
    diagnostics(),
    frames(),
    encodings(),
    statusManagement(statusManagement),
    freeText()
{
}

NodeProperty::NodeProperty(uint8_t type, std::string & freeText) :
    type(type),
    generals(),
    diagnostics(),
    frames(),
    encodings(),
    statusManagement(),
    freeText(freeText)
{
}

std::ostream & operator<<(std::ostream & os, NodeProperty & obj)
{
    switch (obj.type) {
    case 1:
        os << "  general {" << std::endl;

        for (GeneralProperty & generalProperty : obj.generals) {
            os << generalProperty;
        }

        os << "  }" << std::endl;
        break;
    case 2:
        os << "  diagnostic {" << std::endl;

        for (DiagnosticProperty & diagnosticProperty : obj.diagnostics) {
            os << diagnosticProperty;
        }

        os << "  }" << std::endl;
        break;
    case 3:
        os << "  frames {" << std::endl;

        for (std::pair<std::string, Frame> frame : obj.frames) {
            os << frame.second;
        }

        os << "  }" << std::endl;
        break;
    case 4:
        os << "  encoding {" << std::endl;

        for (Encoding & encoding : obj.encodings) {
            os << encoding;
        }

        os << "  }" << std::endl;
        break;
    case 5:
        os << obj.statusManagement;
        break;
    case 6:
        if (!obj.freeText.empty()) {
            os << "  free_text {" << std::endl;
            os << "    " << obj.freeText << std::endl;
            os << "  }" << std::endl;
        }
        break;
    }

    return os;
}

}
}
